package serv.co.documentservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document("image")
@Data
@Builder
public class Image {
	@Id
	private String  id;

	private String name;

	private String type;

	private byte[] image;

	public Image(String id, String name, String type, byte[] image) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.image = image;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
}