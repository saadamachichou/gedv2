package serv.co.documentservice.model;

import lombok.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.awt.*;


@Document("docudb")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder

public class Doc {

    @Id
    private String id;

    @Indexed(name = "indxnm")
    private String name;
    private Image image;;
    private String docx;
    private String checksum;
    private String description;


    public void setName(String name) {
        this.name = name;
    }
    //public void setId(String id) {        this.id = id;}

    public void setImage(Image image) {
        this.image = image;
    }

    public void setDocx(String docx) {
        this.docx = docx;
    }

    public void setChecksum(String docx) {

        this.checksum = DigestUtils.sha256Hex(docx);
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
