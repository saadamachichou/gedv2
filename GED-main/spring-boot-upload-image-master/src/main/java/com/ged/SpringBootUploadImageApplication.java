package com.ged;

import com.ged.connection.Mongoconnect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@SpringBootApplication
@Import({Mongoconnect.class})

public class SpringBootUploadImageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootUploadImageApplication.class, args);
	}

	}